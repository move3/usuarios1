package com.move.ususarios1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ususarios1Application {

    public static void main(String[] args) {
        SpringApplication.run(Ususarios1Application.class, args);
    }

}
